import React from 'react'
import { IconArrowBack, IconTrash, IconSave } from '../../../../assets'

interface IProps {
    children: React.ReactNode
    isUpdate: boolean
    onClose(): void
    onDelete(): void
    onSubmit(): void
    open: boolean
    height?: string
}

const FormModal = (props: IProps) => {
    const { open, isUpdate, onSubmit, onDelete, onClose, children, height } = props
    return (
        <div className={`${open ? 'opacity-100 scale-y-100 ' : 'opacity-0 scale-y-0 '} fixed z-10 inset-0 transition duration-300 ease-in-out md:px-96`} aria-labelledby="modal-title" role="dialog" aria-modal="true">
            <div className="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

                {/* <!-- This element is to trick the browser into centering the modal contents. --> */}
                <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

                <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle w-full">
                    <div className="bg-white px-4 pt-5 pb-4 p-6 pb-4">
                        <div className="flex justify-between items-center ">
                            <div className="my-2 cursor-pointer" onClick={onClose}>
                                <img src={IconArrowBack} alt="ic-arrow-back" />
                            </div>
                            {isUpdate &&
                                <div className="cursor-pointer" onClick={onDelete}>
                                    <img src={IconTrash} alt="" />
                                </div>
                            }
                        </div>
                        {/* Children */}
                        <form onSubmit={onSubmit} className={`overflow-y-auto ${height||'h-auto'}`}>
                                {children}
                            <button
                                className="bg-blue-600 text-white py-3 rounded w-full mt-8 hover:bg-blue-800 transition duration-300"
                                type="submit"
                                >
                                <div className="flex justify-center items-center gap-1">
                                    <div className="">
                                        <img src={IconSave} alt="" />
                                    </div>
                                    <div className="">Simpan</div>
                                </div>
                            </button>
                        </form>
                        {/* /Children */}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FormModal
