import React, { ReactNode } from 'react'

interface ITable {
    header: string[]
    children: ReactNode
}

const Table = ({ header, children }: ITable) => {
    return (
        <div className="overflow-x-auto">
            <table className='w-full'>
                <thead>
                    <tr className='whitespace-nowrap text-primary text-left border-b'>
                        {header.map((item: any, idx: number) => (
                            <th className="px-6 py-2" key={idx}>{item}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {children}
                </tbody>
            </table>
        </div>
    )
}

export default Table
