import { FC } from "react";
import { RootStateOrAny, useSelector } from "react-redux";
import { Link } from "react-router-dom";

interface IProps {
  data: {
    url: string
    title: string
    iconActive: string
    iconDisabled: string
    roles: string[]
  }[]
}

const Menu: FC<IProps> = ({ data }) => {
  const state = useSelector((state: RootStateOrAny) => state)
  return (
    <div className="flex flex-col space-y-6 pt-12 mx-6">
      {data.map((menu, idx) => (
        <Link
          to={menu.url}
          key={idx}
          className={`${window.location.pathname === menu.url
            ? "text-primary"
            : "text-secondary hover:bg-gray-50 group hover:bg-opacity-40 hover:text-white"
            } ${!menu.roles.includes(state.user?.['role']) && 'hidden'} flex items-center space-x-4 font-bold p-2 rounded transition duration-500 ease-in-out`}
        >
          <div>
            <img
              className="opacity-100 group-hover:opacity-0 group-hover:w-0 group-hover:h-0 transition duration-300 ease-in-out"
              src={
                window.location.pathname === menu.url
                  ? menu.iconActive
                  : menu.iconDisabled
              }
              alt="active-or-inactive-icon"
            />
            <img
              className="w-0 h-0 opacity-0 group-hover:opacity-100 group-hover:w-auto group-hover:h-auto transition duration-300 ease-in-out"
              src={menu.iconActive}
              alt="inactive-icon"
            />
          </div>
          <div className="">{menu.title}</div>
        </Link>
      ))}
    </div>
  )
};

export default Menu;
