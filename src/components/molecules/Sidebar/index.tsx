import React, { FC, useState } from "react";
import { IconMenu } from "../../../assets";
import { Header } from "..";
import {
  IconArrowBack,
  IconBill,
  IconBillActive,
  IconHome,
  IconHomeActive,
  Iconpayment,
  IconpaymentActive,
  IconStudent,
  IconStudentActive, ILLogo
} from "../../../assets";
import Menu from "./Menu";

interface ISidebar {
  isOpen: boolean
  handleSidebar(): void
}

const Sidebar = () => {
  const data = [
    {
      url: "/",
      title: "Dashboard",
      iconActive: IconHomeActive,
      iconDisabled: IconHome,
      roles: ['admin', 'student', 'principal']
    },
    {
      url: "/users",
      title: "Pengguna",
      iconActive: IconStudentActive,
      iconDisabled: IconStudent,
      roles: ['admin']
    },
    {
      url: "/students",
      title: "Siswa",
      iconActive: IconStudentActive,
      iconDisabled: IconStudent,
      roles: ['admin']
    },
    {
      url: "/tp",
      title: "Tahun Pelajaran",
      iconActive: IconBillActive,
      iconDisabled: IconBill,
      roles: ['admin']
    },
    {
      url: "/classes",
      title: "Kelas",
      iconActive: IconBillActive,
      iconDisabled: IconBill,
      roles: ['admin']
    },
    {
      url: "/administrasi",
      title: "Jenis Administrasi",
      iconActive: IconBillActive,
      iconDisabled: IconBill,
      roles: ['admin']
    },
    {
      url: "/transactions",
      title: "Riwayat Pembayaran",
      iconActive: IconpaymentActive,
      iconDisabled: Iconpayment,
      roles: ['student']
    },
  ];

  const [openSidebar, setOpenSidebar] = useState(true)

  const handleSidebar = () => {
    setOpenSidebar(!openSidebar)
  }

  return (
    <>
      {openSidebar ?
        <div className={`bg-primary w-80 fixed min-h-screen`}>
          <div>
            <div className="text-right p-6 lg:hidden cursor-pointer" onClick={handleSidebar}>
              <img src={IconArrowBack} alt="" />
            </div>
            <img className="mx-auto mt-6" src={ILLogo} alt="" width={80} height={80} />
            <div className="text-primary text-center mt-4">
              SDIT Al-Manar Pekanbaru
            </div>
          </div>
          <div className="overflow-y-auto h-[35rem] pb-6">
            <Menu data={data} />
          </div>
        </div>
        : <div className="text-right lg:hidden p-6 cursor-pointer" onClick={handleSidebar}>
          <img src={IconMenu} alt="" />
        </div>
      }
    </>
  );
};

export default Sidebar;
