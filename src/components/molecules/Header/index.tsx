import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import { logoutUser } from "../../../api/userApi";
import { IconMenu } from "../../../assets";

const Header = () => {
  const navigate = useNavigate();


  const handleLogout = () => {
    Cookies.remove('acc_token')
    Cookies.remove('user_session')
    logoutUser();
    navigate("/login");
  };
  return (
    <div className="flex justify-end lg:justify-self-end items-center h-full">
      <div className="font-bold text-base p-4 text-white cursor-pointer" onClick={handleLogout}>
        Logout
      </div>
    </div>
  );
};

export default Header;
