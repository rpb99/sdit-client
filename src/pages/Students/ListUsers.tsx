import React from 'react'
import { Table } from '../../components'

const ListUsers = ({ data , handleForm}: any) => {
    const tableHeader = ["Nama Pengguna", "Email"]
    return (
        <div>
            <Table header={tableHeader}>
                {data.map((user: any) => (
                    <tr className="whitespace-nowrap hover:bg-white hover:text-dark cursor-pointer text-secondary transition duration-300 ease-in-out" key={user.id_user} onClick={()=>handleForm(user)}>
                        <td className="px-6 py-2" >{user.nama_user}</td>
                        <td className="px-6 py-2" >{user.email}</td>
                    </tr>
                ))}
            </Table>
        </div>
    )
}

export default ListUsers
