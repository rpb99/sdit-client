import React from 'react'
import { Table } from '../../components'

const ListStudents = ({data, handleForm}:any) => {
    const tableHeader = [
        "NIS", "Nama Siswa", "Kelas", "Email Pengguna"
    ]

    return (
        <Table header={tableHeader}>
            {data.map((student: any,idx:number) => (
                <tr className="whitespace-nowrap hover:bg-white hover:text-dark cursor-pointer text-secondary transition duration-300 ease-in-out" key={student.id_siswa} onClick={() => handleForm(student)}>
                    <td className="px-6 py-2" >{idx+1}</td>
                    <td className="px-6 py-2" >{student.nis}</td>
                    <td className="px-6 py-2" >{student.nama_siswa}</td>
                    <td className="px-6 py-2" >{student.nama_kelas}</td>
                    <td className="px-6 py-2" >{student.email}</td>
                </tr>
            ))}
        </Table>
    )
}

export default ListStudents
