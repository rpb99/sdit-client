import { FormControl, FormHelperText, InputLabel, MenuItem, Select, TextField } from '@mui/material';
import React from 'react';


const StudentForm = ({ formik, classes }: any) => {
    const { errors, handleChange, values } = formik

    const jenisKelamin = [
        {
            value: 'L',
            label: 'Laki-laki'
        },
        {
            value: 'P',
            label: 'Perempuan'
        }
    ]
    return (
        <>
            <div className="flex">
                <TextField
                    className="flex-1"
                    autoFocus
                    margin="dense"
                    id="nama_user"
                    name="nama_user"
                    label="Nama Pengguna"
                    disabled
                    value={values.nama_user}
                />
                <TextField
                    className="flex-1"
                    autoFocus
                    margin="dense"
                    id="email"
                    name="email"
                    label="Email"
                    disabled
                    value={values.email}
                />
            </div>
            <TextField
                autoFocus
                margin="dense"
                id="nis"
                name="nis"
                label="NIS"
                fullWidth
                type="text"
                value={values.nis}
                onChange={handleChange}
                error={!!errors.nis}
                helperText={errors.nis}
            />
            <TextField
                autoFocus
                margin="dense"
                id="nama_siswa"
                name="nama_siswa"
                label="Nama Siswa"
                fullWidth
                type="text"
                value={values.nama_siswa}
                onChange={handleChange}
                error={!!errors.nama_siswa}
                helperText={errors.nama_siswa}
            />
            <FormControl fullWidth error={!!errors.id_kelas} margin="dense">
                <InputLabel id="kelas">Kelas</InputLabel>
                <Select
                    labelId="kelas"
                    label="Kelas"
                    id="kelas"
                    name="id_kelas"
                    value={values.id_kelas}
                    onChange={handleChange}
                >
                    {classes.map((kelas: any) => (
                        <MenuItem key={kelas.id_kelas} value={kelas.id_kelas}>{kelas.nama_kelas}</MenuItem>
                    ))}
                </Select>
                <FormHelperText>{errors.id_kelas}</FormHelperText>
            </FormControl>
            {/* <FormControl fullWidth error={!!errors.jenis_kelamin} margin="dense">
                <InputLabel id="jenis_kelamin">Jenis Kelamin</InputLabel>
                <Select
                    labelId="jenis_kelamin"
                    label="Jenis Kelamin"
                    id="jenis_kelamin"
                    name="jenis_kelamin"
                    value={values.jenis_kelamin}
                    onChange={handleChange}
                >
                    {jenisKelamin.map((item: any, idx: number) => (
                        <MenuItem key={idx} value={item.value}>{item.label}</MenuItem>
                    ))}
                </Select>
                <FormHelperText>{errors.jenis_kelamin}</FormHelperText>
            </FormControl>
            <TextField
                autoFocus
                margin="dense"
                id="telepon"
                name="telepon"
                label="Telepon"
                fullWidth
                type="number"
                value={values.telepon}
                onChange={handleChange}
                error={!!errors.telepon}
                helperText={errors.telepon}
            />
            <TextField
                autoFocus
                margin="dense"
                id="alamat"
                name="alamat"
                label="Alamat"
                fullWidth
                type="text"
                value={values.alamat}
                onChange={handleChange}
                error={!!errors.alamat}
                helperText={errors.alamat}
            /> */}
        </>
    )
}

export default StudentForm
