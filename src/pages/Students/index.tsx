import { Alert } from '@mui/material';
import { useFormik } from 'formik';
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import * as yup from "yup";
import { getClasses, getClassesSelect } from '../../api/classApi';
import { addStudent, deleteStudent, getStudents, updateStudent } from '../../api/studentApi';
import { getUsers } from '../../api/userApi';
import { FormModal } from '../../components';
import ListStudents from './ListStudents';
import ListUsers from './ListUsers';
import StudentForm from './StudentForm';

interface IForm {
  id_siswa: string
  nis: string
  nama_siswa: string
  id_kelas: string
  id_user: string
  nama_user: string
  email: string
}

// interface IStudent {
//   id_siswa: string
//   id_user:string
//   id_kelas
// }

const Students = () => {

  const initialFormState: IForm = {
    id_siswa: "",
    nis: "",
    nama_siswa: "",
    id_kelas: "",
    id_user: "",
    nama_user: "",
    email: ""
  }

  const [showForm, setShowForm] = useState(false)
  const [students, setStudents] = useState([])
  const [classes, setClasses] = useState([])
  const [users, setUsers] = useState([])
  const [form, setForm] = useState(initialFormState)
  const [isSubmitted, setIsSubmitted] = useState(false)
  const [notificationMessage, setNotificationMessage] = useState('Data Berhasil Di Simpan.')
  const [term, setTerm] = useState("")
  const [tabMenu, setTabMenu] = useState("Siswa")


  // Formik initial values
  const initialValues: IForm = {
    id_siswa: form.id_siswa,
    nis: form.nis || "",
    nama_siswa: form.nama_siswa || "",
    id_kelas: form.id_kelas,
    id_user: form.id_user,
    email: form.email,
    nama_user: form.nama_user
  }

  const validationSchema = yup.object({
    nis: yup.string().required("NIS Wajib diisi"),
    nama_siswa: yup.string().required("Nama Siswa Wajib diisi"),
    id_kelas: yup.string().required("Kelas Wajib diisi"),
    id_user: yup.string().required("User Wajib diisi"),
  });

  // Load data from server
  const loadStudents = useCallback(async () => {
    const result = await getStudents(term)
    setStudents(result.data)
  }, [term])

  const loadClasses = useCallback(async () => {
    const res: any = await getClassesSelect()
    res.success && setClasses(res.data)
  }, [])

  const loadUsers = useCallback(async () => {
    const res: any = await getUsers(term)
    res.success && setUsers(res.data)
  }, [term])
  // /Load data from server

  useEffect(() => {
    loadStudents()
    loadClasses()
    loadUsers()
  }, [isSubmitted, loadClasses, loadStudents, loadUsers])

  const handleForm = (item: IForm = initialFormState) => {
    setForm({
      id_siswa: item.id_siswa,
      nis: item.nis,
      nama_siswa: item.nama_siswa,
      id_kelas: item.id_kelas,
      id_user: item.id_user,
      nama_user: item.nama_user,
      email: item.email
    })
    setShowForm(true)
    document.body.style.overflow = 'hidden';
  }

  const onCloseForm = () => {
    setShowForm(false)
    setForm(initialFormState)
    document.body.style.overflow = 'unset';
  }

  const showNotification = () => {
    setIsSubmitted(true)
    setTimeout(() => {
      setIsSubmitted(false)
    }, 5000);
  }

  const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
    let timeout = 1000;
    let searchTimeout;
    clearTimeout(searchTimeout)
    setTimeout(() => {
      setTerm(event.target.value)
    }, timeout);
  }

  const onDelete = async () => {
    const res: any = await deleteStudent(formik.values.id_siswa)
    if (res.success) {
      onCloseForm()
      showNotification()
      setNotificationMessage('Data berhasil di hapus.')
    }
  }

  const onSubmit = async () => {
    let result: any;
    if (formik.values.id_siswa) {
      result = await updateStudent(formik.values, formik.values.id_siswa)
      setNotificationMessage('Data berhasil di ubah.')
    } else {
      result = await addStudent(formik.values)
    }
    
    if (result.success) {
      if (!formik.values.id_siswa) formik.resetForm()
      showNotification()
      onCloseForm()
    }
  }

  const handleTab = (value: string) => {
    setTabMenu(value)
  }


  const formik = useFormik({
    enableReinitialize: true,
    initialValues,
    onSubmit,
    validateOnBlur: false,
    validationSchema,
  });

  // ClassNames
  const notificationClassName = `${isSubmitted ? "scale-x-100" : "scale-x-0"} z-50 transition duration-300 ease-in absolute right-0 top-2`

  return (
    <>
      <div className="z-50 flex justify-between items-center">
        <input type="search" name="" id="" placeholder='Pencarian...' className='p-2 rounded focus:outline-none' onChange={handleSearch} />
        {/* <div className="text-primary border p-2  hover:text-black hover:bg-white rounded transition duration-300 cursor-pointer ease-in-out" onClick={() => handleForm()}>Tambah Data</div> */}
      </div>

      {/* Tab Menu */}
      <div className="flex mt-12">
        {["Siswa", "Pengguna"].map((tab, idx) => (
          <div key={idx} className={`${tab === tabMenu ? "text-dark bg-white" : "text-secondary hover:text-black hover:bg-white"}  transition duration-300 cursor-pointer ease-in-out border p-4 w-44 text-center`} onClick={() => handleTab(tab)}>{tab}</div>
        ))}
      </div>
      {/* /Tab Menu */}

      <div className="mt-16">
        {tabMenu === 'Siswa' ?

          <ListStudents data={students} handleForm={handleForm} />
          :
          <ListUsers data={users} handleForm={handleForm} />
        }
      </div>


      {/* Form */}
      <FormModal
        isUpdate={!!form.id_siswa}
        onClose={onCloseForm}
        onDelete={onDelete}
        onSubmit={formik.handleSubmit}
        open={showForm}
      // height='h-[36rem]'
      >
        <StudentForm
          classes={classes}
          formik={formik}
        />
      </FormModal>

      {/* Modal when submit */}
      <div className={notificationClassName}>
        <Alert severity="success">
          {notificationMessage}
        </Alert>
      </div>
    </>
  )
}

export default Students
