import { Alert } from '@mui/material';
import { useFormik } from 'formik';
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import * as yup from "yup";
import { getClasses, getClassesSelect } from '../../api/classApi';
import { addJenisSPP, deleteJenisSPP, getJenisSPP, updateJenisSPP } from '../../api/sppApi';
import { getTP } from '../../api/tahunPelajaranApi';
import { FormModal, Table } from '../../components';
import { formatCurrency } from '../../utils';
import ClassForm from './SPPForm';

interface IForm {
    id_jenis_administrasi: string
    id_tp: string
    nama_administrasi: string
    nominal: number
    keterangan: string
}

const Administrasi = () => {

    const initialFormState: IForm = {
        id_jenis_administrasi: "",
        id_tp: "",
        nama_administrasi: "",
        nominal: 0,
        keterangan: "",
    }

    const tableHeader = ["Nama SPP", "Nominal", "Keterangan", "Tahun Pelajaran"]

    const [showForm, setShowForm] = useState(false)
    const [classes, setClasses] = useState([])
    const [form, setForm] = useState(initialFormState)
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [notificationMessage, setNotificationMessage] = useState('Data Berhasil Di Simpan.')
    const [term, setTerm] = useState("")
    const [years, setYears] = useState([])
    const [spp, setSpp] = useState([])

    const initialValues: IForm = {
        id_jenis_administrasi: form.id_jenis_administrasi,
        id_tp: form.id_tp,
        nama_administrasi: form.nama_administrasi,
        nominal: form.nominal,
        keterangan: form.keterangan
    }

    const validationSchema = yup.object({
        id_tp: yup.string().required("Tahun Pelajaran Wajib diisi"),
        nama_administrasi: yup.string().required("Nama SPP Wajib diisi"),
        nominal: yup.number().required("Nominal Wajib diisi"),
    });

    const loadClasses = useCallback(async () => {
        const result = await getClassesSelect()
        setClasses(result.data)
    }, [term])

    const loadSPP = useCallback(async () => {
        const result = await getJenisSPP(term)
        setSpp(result.data)
    }, [term])

    const loadYears = async () => {
        const res: any = await getTP()
        res.success && setYears(res.data)
    }


    useEffect(() => {
        loadYears()
        loadClasses()
        loadSPP()
    }, [isSubmitted, loadClasses, loadSPP])

    const handleForm = (item: IForm = initialFormState) => {
        setForm({
            id_jenis_administrasi: item.id_jenis_administrasi,
            id_tp: item.id_tp,
            nama_administrasi: item.nama_administrasi,
            nominal: item.nominal,
            keterangan: item.keterangan
        })
        setShowForm(true)
    }

    const onCloseForm = () => {
        setShowForm(false)
        setForm(initialFormState)
    }

    const showNotification = () => {
        setIsSubmitted(true)
        setTimeout(() => {
            setIsSubmitted(false)
        }, 5000);
    }

    const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let timeout = 1000;
        let searchTimeout;
        clearTimeout(searchTimeout)
        searchTimeout = setTimeout(() => {
            setTerm(event.target.value)
        }, timeout);
    }

    const onDelete = async () => {
        const res: any = await deleteJenisSPP(formik.values.id_jenis_administrasi)
        if (res.success) {
            onCloseForm()
            showNotification()
            setNotificationMessage('Data berhasil di hapus.')
        }
    }

    const onSubmit = async () => {
        let result: any;
        if (formik.values.id_jenis_administrasi) {
            result = await updateJenisSPP(formik.values, formik.values.id_jenis_administrasi)
            setNotificationMessage('Data berhasil di ubah.')
        } else {
            result = await addJenisSPP(formik.values)
        }

        if (result.success) {
            if (!formik.values.id_jenis_administrasi) formik.resetForm()
            showNotification()
        }
    }


    const formik = useFormik({
        enableReinitialize: true,
        initialValues,
        onSubmit,
        validateOnBlur: false,
        validateOnChange: false,
        validationSchema,
    });

    // ClassNames
    const notificationClassName = `${isSubmitted ? "scale-x-100" : "scale-x-0"} z-50 transition duration-300 ease-in absolute right-0 top-2`

    return (
        <div>
            <div className="z-50 flex justify-between items-center">
                <input type="search" name="" id="" placeholder='Pencarian...' className='p-2 rounded focus:outline-none' onChange={handleSearch} />
                <div className="text-primary border p-2  hover:text-black hover:bg-white rounded transition duration-300 cursor-pointer ease-in-out" onClick={() => handleForm()}>Tambah Data</div>
            </div>
            <div className="mt-16">
                <Table header={tableHeader}>
                    {spp.map((item: any) => (
                        <tr className="whitespace-nowrap hover:bg-white hover:text-dark cursor-pointer text-secondary transition duration-300 ease-in-out" key={item.id_jenis_administrasi} onClick={() => handleForm(item)}>
                            <td className="px-6 py-2" >{item.nama_administrasi}</td>
                            <td className="px-6 py-2" >{formatCurrency(item.nominal)}</td>
                            <td className="px-6 py-2" >{item.keterangan}</td>
                            <td className="px-6 py-2" >{item.nama_tp}</td>
                        </tr>
                    ))}
                </Table>
            </div>
            {/* Form */}
            <FormModal
                isUpdate={!!form.id_jenis_administrasi}
                onClose={onCloseForm}
                onDelete={onDelete}
                onSubmit={formik.handleSubmit}
                open={showForm}
            >
                <ClassForm formik={formik} years={years} />
            </FormModal>

            {/* Modal when submit */}
            <div className={notificationClassName}>
                <Alert severity="success">
                    {notificationMessage}
                </Alert>
            </div>
        </div>
    )
}

export default Administrasi
