import { FormControl, FormHelperText, InputLabel, MenuItem, Select, TextField } from '@mui/material';
import React from 'react';


const SPPForm = ({ formik, years }: any) => {
    const { errors, handleChange, values } = formik
    return (
        <>
            <TextField
                autoFocus
                margin="dense"
                id="nama_administrasi"
                name="nama_administrasi"
                label="Nama Administrasi"
                fullWidth
                type="text"
                value={values.nama_administrasi}
                onChange={handleChange}
                error={!!errors.nama_administrasi}
                helperText={errors.nama_administrasi}
            />
            <TextField
                autoFocus
                margin="dense"
                id="nominal"
                name="nominal"
                label="Nominal"
                fullWidth
                type="number"
                value={values.nominal}
                onChange={handleChange}
                error={!!errors.nominal}
                helperText={errors.nominal}
            />
            <TextField
                autoFocus
                margin="dense"
                id="keterangan"
                name="keterangan"
                label="Keterangan"
                fullWidth
                value={values.keterangan}
                onChange={handleChange}
                error={!!errors.keterangan}
                helperText={errors.keterangan}
            />
            <FormControl fullWidth error={!!errors.id_tp} margin="dense">
                <InputLabel id="id_tp">Tahun Pelajaran</InputLabel>
                <Select
                    labelId="id_tp"
                    label="Tahun Pelajaran"
                    id="id_tp"
                    name="id_tp"
                    value={values.id_tp}
                    onChange={handleChange}
                >
                    {years.map((year: any) => (
                        <MenuItem key={year.id_tp} value={year.id_tp}>{year.nama_tp}</MenuItem>
                    ))}
                </Select>
                <FormHelperText>{errors.id_tp}</FormHelperText>
            </FormControl>
        </>
    )
}

export default SPPForm
