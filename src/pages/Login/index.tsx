import { Alert } from "@mui/material";
import TextField from "@mui/material/TextField";
import axios from 'axios';
import { useFormik } from "formik";
import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import { loginUser, currentUser } from "../../api/userApi";
import { ILLogo } from "../../assets";

interface IForm {
  email: string;
  password: string;
}

const initialValues: IForm = {
  email: "",
  password: "",
};

const Login = () => {
  const navigate = useNavigate();
  const [isSubmitted, setIsSubmitted] = useState(false)
  const [notificationMessage, setNotificationMessage] = useState('Data Berhasil Di Simpan.')


  const token = Cookies.get('acc_token')


  const validationSchema = yup.object({
    email: yup.string().required("Email Wajib diisi"),
    password: yup.string().required("Password Wajib diisi"),
  });

  useEffect(() => {
    if (token) return navigate("/");
  }, [token, navigate]);

  const onSubmit = async () => {
    try {
      const sessionExpired = new Date()
      sessionExpired.setTime(sessionExpired.getDate() + 90)
      const inTenSeconds = new Date(new Date().getTime() + 10 * 1000);


      const res: any = await loginUser(formik.values);

      console.log(res);


      Cookies.set("acc_token", res.accessToken, {
        expires: inTenSeconds,
        path: "/",
        secure: true,
      });

      Cookies.set("user_session", "true", {
        expires: 90,
        path: "/",
        secure: true,
      });

      axios.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${res.accessToken}`;

      navigate('/');
      // return () => clearTimeout(timer)
    } catch (error: any) {
      const { status } = error.response
      setIsSubmitted(true)
      setTimeout(() => {
        setIsSubmitted(false)
      }, 5000);
      if (status === 401) setNotificationMessage("Password anda salah.")
      if (status === 404) setNotificationMessage("Email tidak terdaftar.")
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  });

  const notificationClassName = `${isSubmitted ? "scale-x-100" : "scale-x-0"} z-50 transition duration-300 ease-in absolute right-0 top-2`

  return (
    <div className="mx-12">
      <div>
        <div className="text-xl text-center my-6">Selamat Datang</div>
        <img src={ILLogo} width="160" className="m-auto" alt="sdit-almanar-pku" />
      </div>
      <div className="w-full md:w-1/2 mx-auto mt-12">
        <form onSubmit={formik.handleSubmit} className="">
          <TextField
            autoFocus
            margin="dense"
            id="email"
            name="email"
            label="Email"
            fullWidth
            type="text"
            value={formik.values.email}
            onChange={formik.handleChange}
            error={!!formik.errors.email}
            helperText={formik.errors.email}
          />
          <TextField
            margin="dense"
            id="password"
            name="password"
            label="Password"
            fullWidth
            type="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            error={!!formik.errors.password}
            helperText={formik.errors.password}
          />
          <button
            className="bg-blue-600 text-white py-3 rounded w-full mt-8"
            type="submit"
          >
            Login
          </button>
        </form>
      </div>
      <div className={notificationClassName}>
        <Alert severity="error">
          {notificationMessage}
        </Alert>
      </div>
    </div>
  );
};

export default Login;
