import { FormControl, InputLabel, Select, MenuItem } from "@mui/material";
import { Children, useEffect, useState, ChangeEvent, FormEvent, useCallback } from "react";
import { getLogs } from "../../api/paymentLogApi";
import { getTP } from '../../api/tahunPelajaranApi';
import { getClasses } from '../../api/classApi';
import { Table } from "../../components";
import { formatCurrency, formatDate } from "../../utils";
import RenderPDF from "./RenderPDF";
import ReactPDF, { PDFDownloadLink, PDFViewer } from '@react-pdf/renderer';
import { getTransactions } from "../../api/transactionApi";
import { getJenisSPP, getSingleAdministrasi } from "../../api/sppApi";


const PrincipalDashboard = () => {
    const [term, setTerm] = useState("")
    const [transactions, setTransactions] = useState([]);
    const [administrasi, setAdministrasi] = useState([])
    const [total, setTotal] = useState(0)
    // const [years, setYears] = useState([])
    // const [classes, setClasses] = useState([])
    const [filters, setFilters] = useState({
        year: "",
        class: "",
        adm: ""
    })
    const [administration, setAdministration] = useState("")

    useEffect(() => {
        loadSuccessTransactions()
    }, [term])

    useEffect(() => {
        // loadYears()
        // loadClasses()
        loadAdministrasi()
    }, [])

    const loadSuccessTransactions = async () => {
        const res: any = await getTransactions(term, filters.adm, 'ASC')
        if (res.success) {
            setTransactions(res.data)
            setTotal(res.total_price)
        }
    }

    const loadAdministrasi = async () => {
        const res: any = await getJenisSPP()
        res.success && setAdministrasi(res.data)
    }
    // const loadYears = async () => {
    //     const res: any = await getTP()
    //     res.success && setYears(res.data)
    // }

    // const loadClasses = async () => {
    //     const res: any = await getClasses()
    //     res.success && setClasses(res.data)
    // }

    const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let timeout = 1000;
        let searchTimeout;
        clearTimeout(searchTimeout)
        searchTimeout = setTimeout(() => {
            setTerm(event.target.value)
        }, timeout);
    }

    const loadAdministration = async () => {
        const res: any = await getSingleAdministrasi(filters.adm)
        res.success && setAdministration(res.data.nama_administrasi)
    }

    const handleFilter = async (event: FormEvent) => {
        const { name, value } = event.target as HTMLSelectElement
        setFilters(prevState => ({ ...prevState, [name]: value }))
    }

    // const sumTotal = () => {
    //     const price = transactions.reduce((acc: any, curr: any) => {
    //         return curr.nominal + acc
    //     }, 0)
    //     setTotal(price)
    // }

    const submitFilter = () => {
        loadSuccessTransactions()
        loadAdministration()
        // sumTotal()
    }

    const tableHeader = ['NIS', 'Nama Siswa', 'Kelas', 'Nama Administrasi', 'Nominal', 'Tahun Pelajaran', 'Tanggal Bayar']
    return (
        <div>
            {/* <PDFViewer width={'100%'} height={1000}>
                <RenderPDF logs={transactions} filters={filters} administrationName={administration}/>
            </PDFViewer> */}
            <div className="z-50 flex flex-col  md:flex-row justify-between items-center">
                <input type="search" name="" id="" placeholder='Pencarian...' className='p-2 rounded focus:outline-none' onChange={handleSearch} />
                <div className="flex items-center gap-6 mt-12 md:mt-0">
                    <select className="p-2 rounded bg-white" name="adm" id="" onChange={handleFilter}>
                        <option value="">Jenis Administrasi</option>
                        {Children.toArray(administrasi.map((item: any) => (
                            <option value={item.id_jenis_administrasi}>{item.nama_administrasi}</option>
                        )))}
                    </select>
                    {/* <select className="p-2 rounded bg-white" name="year" id="" onChange={handleFilter}>
                        <option value="">Tahun Pelajaran</option>
                        {years.map((year: any, idx) => (
                            <option key={idx} value={year.id_tp}>{year.nama_tp}</option>
                        ))}
                    </select>

                    <select className="p-2 rounded bg-white" name="class" id="" onChange={handleFilter}>
                        <option value="">Kelas</option>
                        {classes.map((year: any, idx) => (
                            <option key={idx} value={year.id_kelas}>{year.nama_kelas}</option>
                        ))}
                    </select> */}
                    <button onClick={submitFilter} className="bg-secondary text-primary p-2 rounded">Apply Filter</button>
                </div>
            </div>
            <div className="flex justify-end mt-6">

                <PDFDownloadLink document={<RenderPDF logs={transactions} filters={filters} administrationName={administration} total={total} />} fileName={`Laporan Pembayaran_${administration}.pdf`} >
                    {({ blob, url, loading, error }) =>
                        loading ? <div className="bg-secondary text-primary p-2 rounded">Loading...</div> : !!filters.adm && <div className="bg-secondary text-primary p-2 rounded">Unduh Laporan</div>
                    }
                </PDFDownloadLink>
            </div>
            <div className="mt-16">
                <Table header={tableHeader}>
                    {transactions.map((trx: any) => (
                        <tr className="whitespace-nowrap text-secondary mt-4" key={trx.id_transaksi}>
                            <td className="px-6 py-2">{trx.nis}</td>
                            <td className="px-6 py-2">{trx.nama_siswa}</td>
                            <td className="px-6 py-2">{trx.nama_kelas}</td>
                            <td className="px-6 py-2">{trx.nama_administrasi}</td>
                            <td className="px-6 py-2">{formatCurrency(trx.nominal)}</td>
                            <td className="px-6 py-2">{trx.nama_tp}</td>
                            <td className="px-6 py-2">{formatDate(trx.tgl_bayar)}</td>
                            {/* 
                            */}
                            {/* <td className="px-6 py-2" onClick={(e) => handlePayment(log.id_transaksi)}><button className='px-6 py-2 bg-white text-dark text-center rounded-lg hover:scale-110 transition duration-300 ease-in-out'>Bayar</button></td> */}
                        </tr>
                    ))}
                </Table>
            </div>
        </div>
    );
};

export default PrincipalDashboard;
