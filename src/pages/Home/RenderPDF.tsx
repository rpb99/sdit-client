import React, { useEffect, useState } from 'react'
import { Page, Text, View, Document, StyleSheet, Image } from '@react-pdf/renderer';
import { ILLogo } from '../../assets';
import { Table, TableHeader, TableCell, TableBody, DataTableCell } from '@david.kucsai/react-pdf-table'
import { formatCurrency, formatDate } from '../../utils';
import { getSingleTP } from '../../api/tahunPelajaranApi';
import { getClass } from '../../api/classApi';
import { getSingleAdministrasi } from '../../api/sppApi';

const styles = StyleSheet.create({
    page: {
        flexDirection: 'row',
        backgroundColor: '#E4E4E4'
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1,
        justifyContent: 'space-between'
    },
    headerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 36,
        borderBottom: '2 solid #000',
        paddingBottom: 24
    },
    logo: {
        width: 120,
        height: 120,
    },
    headerLabelContainer: {
        flexGrow: 1,
        alignItems: 'center',
        fontSize: 18,
    },
    labelAddress: {
        marginTop: 6,
        marginBottom: 12,
        fontSize: 14

    },
    labelHeaderReport: {
        marginTop: 12,
        alignItems: 'center',
    },
    tableContainer: {
        fontSize: 14,
        marginTop: 24
    },
    signatureContainer: {
        fontSize: 11,
        alignItems: 'flex-end',
    },
    signature: {
        marginTop: 36,
        marginBottom: 24
    },
    total: {
        textAlign: 'right',
        border: '1px solid black'
    }
});

const RenderPDF = ({ logs, filters, administrationName, total }: any) => {
    // const [year, setYear] = useState({}) as any
    // const [kelas, setKelas] = useState({}) as any
    const tableHeader = ["No", "NIS", "Nama Siswa", "Tanggal  Bayar", "Nominal"]
    // const [administration, setAdministration] = useState({})
    // useEffect(() => {
    //     loadTP()
    //     loadClass()
    // }, [])


    // useEffect(() => {
    //     loadAdministration()
    // }, [])


    // const loadAdministration = async () => {
    //     const res: any = await getSingleAdministrasi(filters.adm)
    //     res.success && setAdministration(res.data)
    // }
    // const loadTP = async () => {
    //     const res: any = await getSingleTP(filters.year)
    //     res.success && setYear(res.data)
    // }
    // const loadClass = async () => {
    //     const res: any = await getClass(filters.class)
    //     res.success && setKelas(res.data)
    // }

    return (
        <Document>
            <Page size="A4" style={styles.page} orientation="landscape">
                <View style={styles.section}>
                    <View>
                        <View style={styles.headerContainer}>
                            <Image src={ILLogo} style={styles.logo} />
                            <View style={styles.headerLabelContainer}>
                                <Text >YAYASAN PENDIDIKAN ISLAM AL-MANAR</Text>
                                <Text >SEKOLAH DASAR ISLAM TERPADU AL-MANAR</Text>
                                <Text style={styles.labelAddress}>Jl. Duyung Gang Al-Manar No.11</Text>
                                <Text>Laporan Pembayaran Administrasi</Text>
                                <Text>{administrationName}</Text>
                                {/* <Text>Tahun Pelajaran {year[0]?.nama_tp}</Text>
                                <Text>Kelas {kelas[0]?.nama_kelas}</Text> */}
                            </View>
                        </View>

                        <View style={styles.tableContainer}>
                            <Table
                                data={logs}
                            >
                                <TableHeader fontSize={14} textAlign='center'>
                                    {tableHeader.map((label, idx) => (
                                        <TableCell style={{ fontWeight: 'bold', padding: 5 }}>
                                            {label}
                                        </TableCell>
                                    ))}
                                </TableHeader>
                                <TableBody textAlign='center'>
                                    <DataTableCell getContent={(r) => r.no} />
                                    <DataTableCell getContent={(r) => r.nis} />
                                    <DataTableCell getContent={(r) => r.nama_siswa} />
                                    <DataTableCell getContent={(r) => formatDate(r.tgl_bayar, 'dd MMM yyyy')} />
                                    <DataTableCell getContent={(r) => formatCurrency(r.nominal)} />
                                </TableBody>
                            </Table>
                            <View style={styles.total}>
                                <Text>Total: {formatCurrency(total)}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.signatureContainer}>
                        <Text>ANTON SAPUTRA, S.PD.I</Text>
                        <Text style={styles.signature}>(...................................................)</Text>
                        <Text>Pekanbaru, {formatDate(new Date(), "dd/MM/yyyy")}</Text>
                    </View>
                </View>
            </Page>
        </Document>
    )
}

export default RenderPDF
