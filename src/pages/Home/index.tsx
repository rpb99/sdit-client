import { RootStateOrAny, useSelector } from "react-redux";
import AdminDashboard from './AdminDashboard';
import PrincipalDashboard from './PrincipalDashboard';
import StudentDashboard from './StudentDashboard';


const Home = () => {
  const { user } = useSelector((state: RootStateOrAny) => state)

  const renderCheck = () => {
    if (user?.role === 'admin') {
      return <AdminDashboard />
    }

    if (user?.role === 'student') {
      return <StudentDashboard />
    }

    if (user?.role === 'principal') {
      return <PrincipalDashboard />
    }

  }

  return (
    <>
      {renderCheck()}
    </>
  );
};

export default Home;
