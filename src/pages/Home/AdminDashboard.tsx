import { ChangeEvent, useEffect, useState } from "react";
import { getTransactionsAdmin } from "../../api/transactionApi";
import { Table } from "../../components";
import { formatCurrency, formatDate } from "../../utils";


const AdminDashboard = () => {
    const [term, setTerm] = useState("")
    const [transactions, setTransactions] = useState([]);
 
    useEffect(() => {
        loadSuccessTransactions()
    }, [term])

    const loadSuccessTransactions = async () => {
        const res: any = await getTransactionsAdmin(term)
        res.success && setTransactions(res.data)
    }


    const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let timeout = 1000;
        let searchTimeout;
        clearTimeout(searchTimeout)
        searchTimeout = setTimeout(() => {
            setTerm(event.target.value)
        }, timeout);
    }

    const tableHeader = ['Nomor Transaksi', 'NIS', 'Nama Siswa', 'Kelas', 'Nama Administrasi', 'Nominal', 'Tahun Pelajaran', 'Tanggal Bayar']

    return (
        <div>
            <div className="z-50 flex justify-between items-center">
                <input type="search" name="" id="" placeholder='Pencarian...' className='p-2 rounded focus:outline-none' onChange={handleSearch} />
            </div>
            <div className="mt-16">
                <Table header={tableHeader}>
                    {transactions.map((trx: any) => (
                        <tr className="whitespace-nowrap text-secondary mt-4" key={trx.id_transaksi}>
                            <td className="px-6 py-2">{"..." + trx.id_transaksi.split('-').pop()}</td>
                            <td className="px-6 py-2">{trx.nis}</td>
                            <td className="px-6 py-2">{trx.nama_siswa}</td>
                            <td className="px-6 py-2">{trx.nama_kelas}</td>
                            <td className="px-6 py-2">{trx.nama_administrasi}</td>
                            <td className="px-6 py-2">{formatCurrency(trx.nominal)}</td>
                            <td className="px-6 py-2">{trx.nama_tp}</td>
                            <td className="px-6 py-2">{formatDate(trx.tgl_bayar)}</td>
                            {/* <td className="px-6 py-2" onClick={(e) => handlePayment(log.id_transaksi)}><button className='px-6 py-2 bg-white text-dark text-center rounded-lg hover:scale-110 transition duration-300 ease-in-out'>Bayar</button></td> */}
                        </tr>
                    ))}
                </Table>
            </div>
        </div>
    );
};

export default AdminDashboard;
