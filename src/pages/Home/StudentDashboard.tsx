import { add } from 'date-fns';
import is from 'date-fns/esm/locale/is/index.js';
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { RootStateOrAny, useSelector } from "react-redux";
import { addTransaction, getBills, getTransaction } from '../../api/transactionApi';
import { Loading, Table } from '../../components';
import { formatCurrency } from '../../utils';

const StudentDashboard = () => {
    const state = useSelector((state: RootStateOrAny) => state)
    const [bills, setBills] = useState([])
    const [term, setTerm] = useState("")
    const [loading, setLoading] = useState(true);
    const [isNotFound, setIsNotFound] = useState(false);


    const tableHeader = ['Nama Administrasi', 'Nominal', 'Keterangan', 'Tahun Ajaran', '']


    useEffect(() => {
        loadBills()
    }, [term])

    const loadBills = async () => {
        if (!state.student?.id_siswa) return;
        try {
            const res: any = await getBills(state.student?.id_siswa, term)
            if (res.success) {
                setBills(res.data)
                setLoading(false)
            }
            setIsNotFound(false)
        } catch (error: any) {
            error.response.status === 404 && setIsNotFound(true)
            setLoading(false)
        }

    }




    const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let timeout = 1000;
        let searchTimeout;
        clearTimeout(searchTimeout)
        searchTimeout = setTimeout(() => {
            setTerm(event.target.value)
        }, timeout);
    }

    const handlePayment = async (billId: string) => {
        try {
            const transaction: any = await getTransaction(billId)

            let laterDate = add(new Date(transaction.data.updatedAt), {
                hours: 6
            })

            if (!transaction.data.snap_url || new Date().getTime() >= laterDate.getTime()) {
                const bill: any = await addTransaction(billId)
                return window.open(bill.data.snap_url, '_blank')
            }

            return window.open(transaction.data?.snap_url, '_blank')


        } catch (error: any) {
            console.log(error.response)
        }
    }

    return (
        <div>
            <div className="text-secondary font-bold my-12">
                <div className="text-lg">NIS : {state.student?.nis}</div>
                <div className="text-lg">Nama Siswa : {state.student?.nama_siswa}</div>
            </div>

            <div className="z-50 flex justify-between items-center">
                <input type="search" name="" id="" placeholder='Pencarian...' className='p-2 rounded focus:outline-none' onChange={handleSearch} />
            </div>
            <div className="mt-16">
                <Table header={tableHeader}>
                    {bills.length && !isNotFound ?
                        <>
                            {bills.map((bill: any) => (
                                <tr className="whitespace-nowrap text-secondary mt-4" key={bill.id_transaksi}>
                                    <td className="px-6 py-2">{bill.nama_administrasi}</td>
                                    <td className="px-6 py-2">{formatCurrency(bill.nominal)}</td>
                                    <td className="px-6 py-2">{bill.keterangan}</td>
                                    <td className="px-6 py-2">{bill.nama_tp}</td>
                                    <td className="px-6 py-2" onClick={(e) => handlePayment(bill.id_transaksi)}><button className='px-6 py-2 bg-white text-dark text-center rounded-lg hover:scale-110 transition duration-300 ease-in-out'>Bayar</button></td>
                                </tr>
                            ))}
                        </>
                        : ""
                    }
                </Table>
                {loading && <div className="flex justify-center mt-12"><Loading /></div>}
                {isNotFound && <div className="mt-16 text-lg text-center text-secondary font-bold w-full">Data Tidak Ditemukan.</div>}
            </div>
        </div>
    )
}

export default StudentDashboard
