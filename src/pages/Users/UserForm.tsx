import { FormControl, FormHelperText, InputLabel, MenuItem, Select, TextField } from '@mui/material';
import React from 'react';


const UserForm = ({ formik, classes }: any) => {
    const { errors, handleChange, values } = formik

    return (
        <>
            <TextField
                autoFocus
                margin="dense"
                id="nama_user"
                name="nama_user"
                label="Nama Pengguna"
                fullWidth
                value={values.nama_user}
                onChange={handleChange}
                error={!!errors.nama_user}
                helperText={errors.nama_user}
            />
            <TextField
                autoFocus
                margin="dense"
                id="email"
                name="email"
                label="Email"
                fullWidth
                disabled={!!values.id_user}
                value={values.email}
                onChange={handleChange}
                error={!!errors.email}
                helperText={errors.email}
            />
        </>
    )
}

export default UserForm
