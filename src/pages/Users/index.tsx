import { Alert } from '@mui/material';
import { useFormik } from 'formik';
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import * as yup from "yup";
import { addUser, deleteUser, getUsersAvailable, updateUser } from '../../api/userApi';
import { FormModal, Table } from '../../components';
import UserForm from './UserForm';

interface IForm {
    id_user: string
    nama_user: string
    email: string
}


const Users = () => {

    const initialFormState: IForm = {
        id_user: "",
        nama_user: "",
        email: ""
    }

    const [term, setTerm] = useState("")
    const [form, setForm] = useState(initialFormState)
    const [users, setUsers] = useState([])
    const [showForm, setShowForm] = useState(false)
    const tableHeader = ["Nama Pengguna", "Email"]
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [notificationMessage, setNotificationMessage] = useState('Data Berhasil Di Simpan.')

    const showNotification = () => {
        setIsSubmitted(true)
        setTimeout(() => {
            setIsSubmitted(false)
        }, 5000);
    }

    const loadUsers = useCallback(async () => {
        const res: any = await getUsersAvailable(term)
        res.success && setUsers(res.data)
    }, [term])

    useEffect(() => {
        loadUsers()
    }, [loadUsers, term, isSubmitted])

    const handleForm = (item: IForm = initialFormState) => {
        setForm({
            id_user: item.id_user,
            nama_user: item.nama_user,
            email: item.email
        })
        setShowForm(true)
        document.body.style.overflow = 'hidden';
    }

    const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let timeout = 1000;
        let searchTimeout;
        clearTimeout(searchTimeout)
        setTimeout(() => {
            setTerm(event.target.value)
        }, timeout);
    }

    const onCloseForm = () => {
        setShowForm(false)
        setForm(initialFormState)
        document.body.style.overflow = 'unset';
    }

    const onDelete = async () => {
        const res: any = await deleteUser(formik.values.id_user)
        if (res.success) {
            onCloseForm()
            showNotification()
            setNotificationMessage('Data berhasil di hapus.')
        }
    }


    // Formik initial values
    const initialValues: IForm = {
        id_user: form.id_user,
        email: form.email,
        nama_user: form.nama_user
    }

    const onSubmit = async () => {
        let result: any;
        if (formik.values.id_user) {
            result = await updateUser(formik.values)
            setNotificationMessage('Data berhasil di ubah.')
        } else {
            result = await addUser(formik.values)
        }

        if (result.success) {
            if (!formik.values.id_user) formik.resetForm()
            showNotification()
        }
    }

    const validationSchema = yup.object({
        nama_user: yup.string().required("Nama Pengguna Wajib diisi"),
        email: yup.string().required("Email Wajib diisi"),
    });

    const formik = useFormik({
        enableReinitialize: true,
        initialValues,
        onSubmit,
        validateOnBlur: false,
        validationSchema,
    });

    const notificationClassName = `${isSubmitted ? "scale-x-100" : "scale-x-0"} z-50 transition duration-300 ease-in absolute right-0 top-2`

    return (
        <div>
            <div className="z-50 flex justify-between items-center">
                <input type="search" name="" id="" placeholder='Pencarian...' className='p-2 rounded focus:outline-none' onChange={handleSearch} />
                <div className="text-primary border p-2 hover:text-black hover:bg-white rounded transition duration-300 cursor-pointer ease-in-out" onClick={() => handleForm()}>Tambah Data</div>
            </div>
            <div className="mt-16">
                <Table header={tableHeader}>
                    {users.map((user: any) => (
                        <tr className="whitespace-nowrap hover:bg-white hover:text-dark cursor-pointer text-secondary transition duration-300 ease-in-out" key={user.id_user} onClick={() => handleForm(user)}>
                            <td className="px-6 py-2" >{user.nama_user}</td>
                            <td className="px-6 py-2" >{user.email}</td>
                        </tr>
                    ))}
                </Table>
            </div>

            {/* Form */}
            <FormModal
                isUpdate={!!form.id_user}
                onClose={onCloseForm}
                onDelete={onDelete}
                onSubmit={formik.handleSubmit}
                open={showForm}
            >
                <UserForm
                    formik={formik}
                />
            </FormModal>

            {/* Modal when submit */}
            <div className={notificationClassName}>
                <Alert severity="success">
                    {notificationMessage}
                </Alert>
            </div>
        </div>
    )
}

export default Users
