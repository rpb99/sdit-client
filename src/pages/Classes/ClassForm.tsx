import { FormControl, FormHelperText, InputLabel, MenuItem, Select, TextField } from '@mui/material';
import React from 'react';


const ClassForm = ({ formik, years }: any) => {
    const { errors, handleChange, values } = formik
    return (
        <>
            <TextField
                autoFocus
                margin="dense"
                id="nama_kelas"
                name="nama_kelas"
                label="Nama Kelas"
                fullWidth
                type="text"
                value={values.nama_kelas}
                onChange={handleChange}
                error={!!errors.nama_kelas}
                helperText={errors.nama_kelas}
            />
            <FormControl fullWidth error={!!errors.id_tp} margin="dense">
                <InputLabel id="id_tp">Tahun Pelajaran</InputLabel>
                <Select
                    labelId="id_tp"
                    label="Tahun Pelajaran"
                    id="id_tp"
                    name="id_tp"
                    value={values.id_tp}
                    onChange={handleChange}
                >
                    {years.map((year: any) => (
                        <MenuItem key={year.id_tp} value={year.id_tp}>{year.nama_tp}</MenuItem>
                    ))}
                </Select>
                <FormHelperText>{errors.id_tp}</FormHelperText>
            </FormControl>

            {/* <TextField
                margin="dense"
                id="id_tp"
                name="id_tp"
                label="Tahun Pelajaran"
                fullWidth
                type="text"
                value={values.id_tp}
                onChange={handleChange}
                error={!!errors.id_tp}
                helperText={errors.id_tp}
            /> */}
        </>
    )
}

export default ClassForm
