import { Alert } from '@mui/material';
import { useFormik } from 'formik';
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import * as yup from "yup";
import { addClass, deleteClass, getClasses, updateClass } from '../../api/classApi';
import { getTP } from '../../api/tahunPelajaranApi';
import { FormModal, Table } from '../../components';
import ClassForm from './ClassForm';

interface IForm {
    id_kelas: string
    id_tp: string
    nama_kelas: string
}

const Classes = () => {

    const initialFormState: IForm = {
        id_kelas: "",
        id_tp: "",
        nama_kelas: ""
    }

    const [showForm, setShowForm] = useState(false)
    const [classes, setClasses] = useState([])
    const [form, setForm] = useState(initialFormState)
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [notificationMessage, setNotificationMessage] = useState('Data Berhasil Di Simpan.')
    const [term, setTerm] = useState("")
    const [years, setYears] = useState([])

    const tableHeader = ["Nama Kelas", 
    // "Tahun Pelajaran"
]

    const initialValues: IForm = {
        id_kelas: form.id_kelas,
        id_tp: form.id_tp,
        nama_kelas: form.nama_kelas
    }

    const validationSchema = yup.object({
        id_tp: yup.string().required("Tahun Pelajaran Wajib diisi"),
        nama_kelas: yup.string().required("Nama Kelas Wajib diisi"),
    });

    const loadClasses = useCallback(async () => {
        const result = await getClasses(term)
        setClasses(result.data)
    }, [term])

    const loadYears = async () => {
        const res: any = await getTP()
        res.success && setYears(res.data)
    }


    useEffect(() => {
        loadYears()
        loadClasses()
    }, [isSubmitted, term, loadClasses])

    const handleForm = (item: IForm = initialFormState) => {
        setForm({ id_kelas: item.id_kelas, id_tp: item.id_tp, nama_kelas: item.nama_kelas })
        setShowForm(true)
    }

    const onCloseForm = () => {
        setShowForm(false)
        setForm(initialFormState)
    }

    const showNotification = () => {
        setIsSubmitted(true)
        setTimeout(() => {
            setIsSubmitted(false)
        }, 5000);
    }

    const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let timeout = 1000;
        let searchTimeout;
        clearTimeout(searchTimeout)
        searchTimeout = setTimeout(() => {
            setTerm(event.target.value)
        }, timeout);
    }

    const onDelete = async () => {
        const res: any = await deleteClass(formik.values.id_kelas)
        if (res.success) {
            onCloseForm()
            showNotification()
            setNotificationMessage('Data berhasil di hapus.')
        }
    }

    const onSubmit = async () => {
        let result: any;
        if (formik.values.id_kelas) {
            result = await updateClass(formik.values, formik.values.id_kelas)
            setNotificationMessage('Data berhasil di ubah.')
        } else {
            result = await addClass(formik.values)
        }

        if (result.success) {
            if (!formik.values.id_kelas) formik.resetForm()
            showNotification()
        }
    }


    const formik = useFormik({
        enableReinitialize: true,
        initialValues,
        onSubmit,
        validateOnBlur: false,
        validateOnChange: false,
        validationSchema,
    });

    // ClassNames
    const notificationClassName = `${isSubmitted ? "scale-x-100" : "scale-x-0"} z-50 transition duration-300 ease-in absolute right-0 top-2`

    return (
        <div>
            <div className="z-50 flex justify-between items-center">
                <input type="search" name="" id="" placeholder='Pencarian...' className='p-2 rounded focus:outline-none' onChange={handleSearch} />
                <div className="text-primary border p-2  hover:text-black hover:bg-white rounded transition duration-300 cursor-pointer ease-in-out" onClick={() => handleForm()}>Tambah Data</div>
            </div>

            <div className="mt-16">
                <Table header={tableHeader}>
                    {classes.map((item: any) => (
                        <tr className="whitespace-nowrap hover:bg-white hover:text-dark cursor-pointer text-secondary transition duration-300 ease-in-out" key={item.id_kelas} onClick={() => handleForm(item)}>
                            <td className="px-6 py-2">{item.nama_kelas}</td>
                            {/* <td className="px-6 py-2">{item.nama_tp}</td> */}
                        </tr>
                    ))}
                </Table>
            </div>

            {/* Form */}
            <FormModal
                isUpdate={!!form.id_kelas}
                onClose={onCloseForm}
                onDelete={onDelete}
                onSubmit={formik.handleSubmit}
                open={showForm}
            >
                <ClassForm formik={formik} years={years} />
            </FormModal>

            {/* Modal when submit */}
            <div className={notificationClassName}>
                <Alert severity="success">
                    {notificationMessage}
                </Alert>
            </div>
        </div>
    )
}

export default Classes
