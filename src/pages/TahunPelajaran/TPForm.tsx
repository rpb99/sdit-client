import { TextField } from '@mui/material';
import React from 'react';


const TPForm = ({ formik }: any) => {
    const { errors, handleChange, values } = formik
    return (
        <TextField
            autoFocus
            margin="dense"
            id="nama_tp"
            name="nama_tp"
            label="Nama Tahun Pelajaran"
            fullWidth
            type="text"
            value={values.nama_tp}
            onChange={handleChange}
            error={!!errors.nama_tp}
            helperText={errors.nama_tp}
        />
    )
}

export default TPForm
