import { Alert } from '@mui/material';
import { useFormik } from 'formik';
import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import * as yup from "yup";
import { addTP, deleteTP, getTP, updateTP } from '../../api/tahunPelajaranApi';
import { FormModal, Table } from '../../components';
import { formatDate } from '../../utils';
import TPForm from './TPForm';

interface IForm {
    id_tp: string
    nama_tp: string
}

const TahunAjaran = () => {

    const initialFormState: IForm = {
        id_tp: "",
        nama_tp: ""
    }

    const [showForm, setShowForm] = useState(false)
    const [tahunPelajaran, setTahunPelajaran] = useState([])
    const [form, setForm] = useState(initialFormState)
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [notificationMessage, setNotificationMessage] = useState('Data Berhasil Di Simpan.')
    const [term, setTerm] = useState("")

    const tableHeader = ["Tahun Pelajaran"]

    const initialValues: IForm = {
        id_tp: form.id_tp,
        nama_tp: form.nama_tp,
    }

    const validationSchema = yup.object({
        nama_tp: yup.string().required("Nama Tahun Ajaran Wajib diisi"),
    });

    const loadClasses = useCallback(async () => {
        const result = await getTP(term)
        setTahunPelajaran(result.data)
    }, [term])


    useEffect(() => {
        loadClasses()
    }, [isSubmitted, term, loadClasses])

    const handleForm = (item: IForm = initialFormState) => {
        setForm({ id_tp: item.id_tp, nama_tp: item.nama_tp })
        setShowForm(true)
    }

    const onCloseForm = () => {
        setShowForm(false)
        setForm(initialFormState)
    }

    const showNotification = () => {
        setIsSubmitted(true)
        setTimeout(() => {
            setIsSubmitted(false)
        }, 5000);
    }

    const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let timeout = 1000;
        let searchTimeout;
        clearTimeout(searchTimeout)
        setTimeout(() => {
            setTerm(event.target.value)
        }, timeout);
    }

    const onDelete = async () => {
        const res: any = await deleteTP(formik.values.id_tp)
        if (res.success) {
            onCloseForm()
            showNotification()
            setNotificationMessage('Data berhasil di hapus.')
        }
    }

    const onSubmit = async () => {
        let result: any;
        if (formik.values.id_tp) {
            result = await updateTP(formik.values, formik.values.id_tp)
            setNotificationMessage('Data berhasil di ubah.')
        } else {
            result = await addTP(formik.values)
        }

        if (result.success) {
            if (!formik.values.id_tp) formik.resetForm()
            showNotification()
        }
    }


    const formik = useFormik({
        enableReinitialize: true,
        initialValues,
        onSubmit,
        validationSchema,
    });

    // ClassNames
    const notificationClassName = `${isSubmitted ? "scale-x-100" : "scale-x-0"} z-50 transition duration-300 ease-in absolute right-0 top-2`

    return (
        <div>
            <div className="z-50 flex justify-between items-center">
                <input type="search" name="" id="" placeholder='Pencarian...' className='p-2 rounded focus:outline-none' onChange={handleSearch} />
                <div className="text-primary border p-2  hover:text-black hover:bg-white rounded transition duration-300 cursor-pointer ease-in-out" onClick={() => handleForm()}>Tambah Data</div>
            </div>
            <div className="mt-16">
                <Table header={tableHeader}>
                    {tahunPelajaran.map((item: any) => (
                        <tr className="whitespace-nowrap hover:bg-white hover:text-dark cursor-pointer text-secondary transition duration-300 ease-in-out" key={item.tp} onClick={() => handleForm(item)}>
                            <td className="px-6 py-2">{item.nama_tp}</td>
                        </tr>
                    ))}
                </Table>
            </div>

            {/* Form */}
            <FormModal
                isUpdate={!!form.id_tp}
                open={showForm}
                onClose={onCloseForm}
                onDelete={onDelete}
                onSubmit={formik.handleSubmit}
            >
                <TPForm formik={formik} />
            </FormModal>

            {/* Notification when submit */}
            <div className={notificationClassName}>
                <Alert severity="success">
                    {notificationMessage}
                </Alert>
            </div>
        </div>
    )
}

export default TahunAjaran
