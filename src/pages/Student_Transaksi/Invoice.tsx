import { Document, Image, Page, StyleSheet, Text, View } from '@react-pdf/renderer';
import React from 'react';
import { ILLogo } from '../../assets';
import { formatCurrency, formatDate } from '../../utils';


const styles = StyleSheet.create({
    page: {
        flexDirection: 'row',
        backgroundColor: '#E4E4E4',
    },
    container: {
        margin: 10,
        padding: 10,
        flexGrow: 1,
    },
    headerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 36,
        borderBottom: '2 solid #000',
        paddingBottom: 24
    },
    logo: {
        width: 196,
        height: 186,
        position: 'absolute',
        top: 60,
        left: 130,
        opacity: 0.1,
    },
    headerLabelContainer: {
        alignItems: 'center',
        fontSize: 14,
    },
    labelAddress: {
        marginTop: 6,
        marginBottom: 8,
        fontSize: 11,
    },
    labelHeaderReport: {
        marginTop: 12,
        alignItems: 'center',
    },
    content: {
        marginTop: 16,
        fontSize: 11,
        color: '#242750'
    },
    priceContainer: {
        paddingTop: 12,
        flexDirection: 'row',
        borderTop: '1 solid #242750',
        justifyContent: 'space-between'
    },
    details: {
        marginBottom: 24
    },
    itemLabel: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: 120
    },
    itemContainer: {
        flexDirection: 'row',
    }

});

interface IGap {
    height?: number
    width?: number
}

interface IItem {
    label: string
    value: string | number | Date | boolean
}

interface IInvoice {
    transaction: any
}

const Gap: React.FC<IGap> = ({ height, width }) => <View style={{ height, width }} />

const Item: React.FC<IItem> = ({ label, value }) => {
    return (
        <View style={styles.itemContainer}>
            <View style={styles.itemLabel}>
                <Text>{label}</Text>
                <Text>:</Text>
            </View>
            <Gap width={12} />
            <Text>{value}</Text>
        </View>
    )
}

const Invoice: React.FC<IInvoice> = ({ transaction }) => {

    return (
        <Document>
            <Page size={[465, 330]} style={styles.page}>
                <Image src={ILLogo} style={styles.logo} />
                <Text>
                    {/* {JSON.stringify(transaction)} */}
                </Text>
                <View style={styles.container}>
                    <View style={styles.headerLabelContainer}>
                        <Text >YAYASAN PENDIDIKAN ISLAM AL-MANAR</Text>
                        <Text >SEKOLAH DASAR ISLAM TERPADU AL-MANAR</Text>
                        <Text style={styles.labelAddress}>Jl. Duyung Gang Al-Manar No.11</Text>
                        <Text>Pembayaran Administrasi Sekolah</Text>
                    </View>

                    <View style={styles.content}>
                        <View style={styles.details}>
                            <Item label="Status" value={transaction.status === 'success' && 'BERHASIL'} />
                            <Gap height={12} />
                            <Item label="No. Transaksi" value={transaction.id_transaksi} />
                            <Gap height={12} />
                            <Item label="Tanggal Pembayaran" value={formatDate(transaction.tgl_bayar)} />
                            <Gap height={12} />
                            <Item label="Pembayaran" value={transaction.nama_administrasi} />
                            <Gap height={12} />
                            <Item label="Nama Siswa" value={transaction.student.nama_siswa} />
                            <Gap height={12} />
                            <Item label="NIS" value={transaction.student.nis} />
                            <Gap height={12} />
                        </View>
                        <View style={styles.priceContainer}>
                            <Text>TOTAL DIBAYAR</Text>
                            <Text>{formatCurrency(transaction.nominal)}</Text>
                        </View>
                    </View>
                </View>
            </Page>
        </Document>
    )
}

export default Invoice
