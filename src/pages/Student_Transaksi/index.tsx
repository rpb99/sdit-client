import { PDFDownloadLink, PDFViewer } from '@react-pdf/renderer';
import React, { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { RootStateOrAny, useSelector } from "react-redux";
import { getTransactionsSuccess } from '../../api/transactionApi';
import { Table } from '../../components';
import { formatCurrency, formatDate } from '../../utils';
import Invoice from './Invoice';

const StudentTransaksi = () => {
    const [student, setStudent] = useState(null) as any
    const [transactions, setTransactions] = useState([])
    const [term, setTerm] = useState("")
    const state = useSelector((state: RootStateOrAny) => state)

    const loadTransactions = useCallback(async () => {
        if (!state.student?.id_siswa) return
        const res: any = await getTransactionsSuccess(state.student?.id_siswa, term)
        res.success && setTransactions(res.data)
    }, [term])

    useEffect(() => {
        loadTransactions()
        return () => {
            setTransactions([])
        }
    }, [loadTransactions])



    const tableHeader = ['Nama Administrasi', 'Nominal', 'Keterangan', 'Tahun Ajaran', 'Tanggal Pembayaran', '']


    const handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
        let timeout = 1000;
        let searchTimeout;
        clearTimeout(searchTimeout)
        searchTimeout = setTimeout(() => {
            setTerm(event.target.value)
        }, timeout);
    }

    return (
        // <PDFViewer width={'100%'} height={1000}>
        //     <Invoice />
        // </PDFViewer>
        <div>
            <div className="text-secondary font-bold my-12">
                <div className="text-lg">NIS : {state.student?.nis}</div>
                <div className="text-lg">Nama Siswa : {state.student?.nama_siswa}</div>
            </div>

            <div className="z-50 flex justify-between items-center">
                <input type="search" name="" id="" placeholder='Pencarian...' className='p-2 rounded focus:outline-none' onChange={handleSearch} />
            </div>
            <div className="mt-16">
                <Table header={tableHeader}>
                    {transactions.length ?
                        <>
                            {transactions.map((transaction: any) => (
                                <tr className="whitespace-nowrap text-secondary mt-4" key={transaction.id_transaksi}>
                                    <td className="px-6 py-2">{transaction.nama_administrasi}</td>
                                    <td className="px-6 py-2">{formatCurrency(transaction.nominal)}</td>
                                    <td className="px-6 py-2">{transaction.keterangan}</td>
                                    <td className="px-6 py-2">{transaction.tahun_ajaran}</td>
                                    <td className="px-6 py-2">{formatDate(transaction.tgl_bayar)}</td>
                                    <td className="px-6 py-2">
                                        <PDFDownloadLink document={<Invoice transaction={{ ...transaction, student: state.student }} />} fileName={`Invoice_${transaction.nama_administrasi}_${transaction.id_transaksi.split('-').pop()}.pdf`} >
                                            {({ blob, url, loading, error }) =>
                                                loading ? <div className="bg-secondary text-primary p-2 rounded text-center">Loading...</div> : <div className="bg-secondary text-primary p-2 rounded text-center transition duration-100 ease-in-out hover:scale-110">Unduh</div>
                                            }
                                        </PDFDownloadLink>
                                    </td>
                                </tr>
                            ))}
                        </> : ""}
                </Table>
            </div>
            {!transactions.length ? <div className="mt-16 text-lg text-center text-secondary font-bold w-full">Data Tidak Ditemukan.</div> : ""}
        </div>
    )
}

export default StudentTransaksi
