import React from "react";
import { RootStateOrAny, useSelector } from "react-redux";
import { Link } from "react-router-dom";

const NotFound = () => {
  const state = useSelector((state:RootStateOrAny) => state)
  return (
    <div className="bg-primary text-primary min-h-screen flex flex-col items-center justify-center">
      <div className="flex flex-col">
        <div className="text-xl">Oh no! We lost too far...</div>
        <div className="text-8xl">404 NOT FOUND</div>
        {/* <Link className="text-xl self-end border-b" to={state.user?.['role'] === 'admin' ? "/" : "/student-tagihan"}>Hey, Save Me</Link> */}
        <Link className="text-xl self-end border-b" to="/">Hey, Save Me</Link>
      </div>
    </div>
  );
};

export default NotFound;
