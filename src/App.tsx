import Cookies from "js-cookie";
import { FC, useCallback, useEffect, useState } from "react";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { Navigate, Outlet, Route, Routes, useNavigate } from "react-router-dom";
import axiosInstance from "./api/apiClient";
import { getStudent } from "./api/studentApi";
import { currentUser } from "./api/userApi";
import { Header, Sidebar } from "./components";
// Pages
import NotFound from "./pages/404NotFound";
import Classes from "./pages/Classes";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Administrasi from "./pages/Administrasi";
import StudentReport from "./pages/StudentReport";
import Students from "./pages/Students";
import StudentTransaksi from "./pages/Student_Transaksi";
import TahunAjaran from "./pages/TahunPelajaran";
import Users from "./pages/Users";


function App() {
  const session = Cookies.get('user_session')
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const state = useSelector((state: RootStateOrAny) => state)
  const [currentRole, setCurrentRole] = useState("");

  const [openSidebar, setOpenSidebar] = useState(false)

  const handleSidebar = () => {
    setOpenSidebar(!openSidebar)
  }


  const loadCurrentUser = useCallback(async () => {
    const user = await currentUser();
    setCurrentRole(user?.data.role)
    dispatch({ type: "LOGGED_IN_USER", payload: user?.data });
    if (user.data.role === 'student') {
      const student = await getStudent(user.data.id_user)

      dispatch({ type: "FETCH_CURRENT_STUDENT", payload: student?.data });
      // dispatch({ type: "FETCH_BILLS", payload: transactions?.data });
    }
  }, [dispatch]);

  useEffect(() => {
    if (!session) return navigate("/login");
    loadCurrentUser();
    refreshToken()
  }, [loadCurrentUser, navigate, session]);

  const refreshToken = async () => {
    try {
      const res: any = await axiosInstance.get('/refresh-tokens')
      const inTenSeconds = new Date(new Date().getTime() + 10 * 1000);

      Cookies.set("acc_token", res.accessToken, {
        expires: inTenSeconds,
        path: "/",
        secure: true,
      });

    } catch (error: any) {
      console.log(error.response)
    }
  }

  // const handleChange = (event:ChangeEvent<HTMLInputElement>) => {
  //   // Logic Here
  // }



  const Layout = () => {
    return (
      <div className="bg-primary">
        <Sidebar />
        <div className="flex flex-col flex-1">
          <Header />
          <div className="ml-0 lg:ml-80 p-5 min-h-screen"><Outlet /></div>
        </div>
      </div>
    )
  };

  const routes = [
    { path: '/', component: <Home />, roles: ['admin', 'student', 'principal'], title: "Dashboard" },
    { path: '/users', component: <Users />, roles: ['admin'], title: "Pengguna" },
    { path: '/students', component: <Students />, roles: ['admin'], title: "Siswa" },
    { path: '/tp', component: <TahunAjaran />, roles: ['admin'], title: "Tahun Pelajaran" },
    { path: '/classes', component: <Classes />, roles: ['admin'], title: "Kelas" },
    { path: '/administrasi', component: <Administrasi />, roles: ['admin'], title: "Jenis SPP" },
    { path: '/transactions', component: <StudentTransaksi />, roles: ['student'], title: "Transaksi" },
  ]

  const PrivateRoute: FC<{ children: any, roles: any, title: string }> = ({ children, roles, title }) => {
    if (!session) navigate("/login");

    useEffect(() => {
      document.title = title
    }, [])

    if (!roles.includes(state.user?.['role'])) {
      <Navigate to="/not-found" replace />
    }

    return children
  };



  return (
    <div className="flex flex-col flex-1">
      <Routes>
        <Route element={<Layout />}>
          {routes.map((route, idx) =>
            <Route
              key={idx}
              path={route.path}
              element={
                <PrivateRoute roles={route.roles} title={route.title}>
                  {route.component}
                </PrivateRoute>
              }
            />
          )}
        </Route>
        <Route path="/login" element={<Login />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}




export default App;
