type Action = { type: string; payload: {} };

export const studentReducer: any = (state = null, action: Action) => {
    switch (action.type) {
        case "FETCH_CURRENT_STUDENT":
            return action.payload || null;
        default:
            return state;
    }
};
