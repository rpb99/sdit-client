type Action = { type: string; payload: {} };

export const userReducer:any = (state = null, action: Action) => {
  switch (action.type) {
    case "LOGGED_IN_USER":
      return action.payload||null;
    case "LOGOUT":
      return action.payload;
    default:
      return state;
  }
};
