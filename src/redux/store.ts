import { combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { userReducer } from "./userReducer";
import { studentReducer } from "./studentReducer";
import { billReducer } from "./billReducer";

const rootReducer: any = combineReducers({
  user: userReducer,
  student: studentReducer,
  bills: billReducer
});

const store = createStore(rootReducer, composeWithDevTools());

export default store;
