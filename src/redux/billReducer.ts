type Action = { type: string; payload: [] };

export const billReducer: any = (state = null, action: Action) => {
    switch (action.type) {
        case "FETCH_BILLS":
            return action.payload || null;
        default:
            return state;
    }
};
