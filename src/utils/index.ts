import { format } from 'date-fns'
import { id } from 'date-fns/locale';

export function formatDate(date: any, formatDate?: string) {
   return format(new Date(date), !!formatDate ? formatDate : 'dd MMM yyyy HH:mm:ss', { locale: id })
}

export function formatCurrency(currency: number) {
   return "Rp. " + currency.toLocaleString('id')
}