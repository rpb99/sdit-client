const mainColors = {
    purple1: '#333867',
    purple2: '#181A30',
    white1: '#F4F4F5',
    grey1: '#939393' 
}

export const colors = {
    primary: mainColors.purple1,
    secondary: mainColors.purple2,
    text: {
        primary: mainColors.white1,
        secondary: mainColors.grey1
    }
}