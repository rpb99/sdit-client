import IconSortDown from './ic-sort-down.svg'
import IconStudent from './ic-student.svg'
import IconStudentActive from './ic-student-active.svg'
import IconBill from './ic-bill.svg'
import IconBillActive from './ic-bill-active.svg'
import IconHome from './ic-home.svg'
import IconHomeActive from './ic-home-active.svg'
import Iconpayment from './ic-payment.svg'
import IconpaymentActive from './ic-payment-active.svg'
import IconTeacher from './ic-teacher.svg'
import IconTeacherActive from './ic-teacher-active.svg'
import IconArrowBack from './ic-arrow-back.svg'
import IconTrash from './ic-trash.svg'
import IconSave from './ic-save.svg'
import IconMenu from './ic-menu.svg'

export {
    IconStudent,
    IconStudentActive,
    IconBill,
    IconBillActive,
    IconHome,
    IconHomeActive,
    Iconpayment,
    IconpaymentActive,
    IconTeacher,
    IconTeacherActive,
    IconSortDown,
    IconArrowBack,
    IconTrash,
    IconSave,
    IconMenu
}

