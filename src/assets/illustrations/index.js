import ILLogin from './login.svg'
import ILLogo from './logo.png'
import ILLoading from './loading.gif'

export { ILLogin, ILLogo, ILLoading }