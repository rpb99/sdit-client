import apiClient from "./apiClient";

export const getLogs = (keyword: string = "") => apiClient.get(`/logs?s=${keyword.toLowerCase()}`);