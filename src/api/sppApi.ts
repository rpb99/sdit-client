import apiClient from "./apiClient";

export const getJenisSPP = (keyword: string = "") => apiClient.get(`/jenis-administrasi?s=${keyword}`);
export const getSingleAdministrasi = (id: string) => apiClient.get(`/jenis-administrasi/${id}`);
export const addJenisSPP = (form: any) => apiClient.post("/jenis-administrasi", form);
export const updateJenisSPP = (form: any, id: string) => apiClient.put(`/jenis-administrasi/${id}`, form);
export const deleteJenisSPP = (id: string) => apiClient.delete(`/jenis-administrasi/${id}`);
