import apiClient from "./apiClient";

export const getStudents: any = (keyword: string = "") => apiClient.get(`/students?nama=${keyword}&nis=${keyword}`)
export const getStudent: any = (userId: string) => apiClient.get(`/students/${userId}`)
export const addStudent = (studentForm: any) => apiClient.post('/students', studentForm)
export const updateStudent = (studentForm: any, studentId: string) => apiClient.put(`/students/${studentId}`, studentForm)
export const deleteStudent = (studentId: string) => apiClient.delete(`/students/${studentId}`)