import apiClient from "./apiClient";

interface IProps {
    id_tp: string
    nama_tp: string
}


export const addTP = async (form: IProps) => await apiClient.post("/tahun-pelajaran", form);
export const updateTP = (form: IProps, id: string) => apiClient.put(`/tahun-pelajaran/${id}`, form);
export const deleteTP = (id: string) => apiClient.delete<any>(`/tahun-pelajaran/${id}`);
export const getTP = (keyword: string = "") => apiClient.get(`/tahun-pelajaran?s=${keyword}`)
export const getSingleTP = (yearId: string) => apiClient.get(`/tahun-pelajaran/${yearId}`)