import axiosInstance from "./apiClient";

export const loginUser = (form: any) => axiosInstance.post("/login", form);
export const logoutUser = () => axiosInstance.delete("/logout");
export const currentUser = () => axiosInstance.get("/current-user")
export const getUsers = (nama: string = "") => axiosInstance.get(`/users?nama=${nama}`)
export const getUsersAvailable = (nama: string) => axiosInstance.get(`/users/available?nama=${nama}`)
export const deleteUser = (userId: string) => axiosInstance.delete(`/users/${userId}`)
export const addUser = (form: any) => axiosInstance.post('/register', form)
export const updateUser = (form: any) => axiosInstance.put('/profile', form)