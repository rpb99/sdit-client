import apiClient from "./apiClient";

interface IProps {
    id_tp: string
    nama_kelas: string
}


export const addClass = (form: IProps) => apiClient.post("/kelas", form);
export const updateClass = (form: IProps, id: string) => apiClient.put(`/kelas/${id}`, form);
export const deleteClass = (id: string) => apiClient.delete<any>(`/kelas/${id}`);
export const getClasses = (keyword: string="") => apiClient.get(`/kelas?s=${keyword}`)
export const getClassesSelect = () => apiClient.get(`/kelas/select`)
export const getClass = (classId: string) => apiClient.get(`/kelas/${classId}`)