import axios from "axios";
import Cookies from "js-cookie";

// let token: any = localStorage.getItem('acc_token')
let token = Cookies.get('acc_token')
const baseURL = "http://localhost:8000/api"

const axiosInstance = axios.create({
  baseURL,
  headers: {
    Authorization: `Bearer ${token}`
  },
  withCredentials: true,
});



axiosInstance.interceptors.request.use(
  async (config: any) => {
    const session = Cookies.get('user_session')
    if (session) {
      const inTenSeconds = new Date(new Date().getTime() + 10 * 1000);
      const res = await axios.get(`${baseURL}/refresh-tokens`);
      Cookies.set("acc_token", res.data.accessToken, {
        expires: inTenSeconds,
        path: "/",
        secure: true,
      });
      config.headers = {
        'Authorization': `Bearer ${res.data.accessToken}`,
      }
    }

    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(response => {
  return response.data
}, async (error) => {
  const options = {
    path: "/",
    secure: true,
  }
  const originalRequest = error.config;

  if (error.response?.status === 403) {
    Cookies.remove('user_session')
    Cookies.remove('acc_token')
  }


  if (error.response?.status === 401 && !originalRequest._retry) {
    originalRequest._retry = true;
    const inTenSeconds = new Date(new Date().getTime() + 10 * 1000);
    const sessionExpired = new Date()
    sessionExpired.setTime(sessionExpired.getDate() + 90)
    const session = Cookies.get('user_session')
    if (session) {
      const res = await axios.get(`${baseURL}/refresh-tokens`);

      Cookies.set("acc_token", res.data.accessToken, {
        expires: inTenSeconds,
        ...options
      });

      axios.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.accessToken;

    }
    
    Cookies.set("user_session", "true", {
      expires: 90,
      ...options
    });

  }
  return Promise.reject(error);
})


export default axiosInstance;
