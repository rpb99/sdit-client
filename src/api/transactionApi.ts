import apiClient from "./apiClient";

// export const getTransactions = (keyword: string, kelas: string, tp: string, orderBy?:string) => apiClient.get(`/transactions?s=${keyword}&kelas=${kelas}&tp=${tp}&orderBy=${orderBy}`);
export const getTransactions = (keyword: string, adm: string, orderBy?: string) => apiClient.get(`/transactions?s=${keyword}&adm=${adm}&orderBy=${orderBy}`);
export const getTransactionsAdmin = (keyword: string) => apiClient.get(`/transactions?s=${keyword}`);
export const addTransaction = (transactionId: string) => apiClient.post(`/transactions`, { id_transaksi: transactionId });
export const getTransaction = (transactionId: string) => apiClient.get(`/transactions/${transactionId}`);
export const getTransactionsSuccess = (studentId: string, keyword: string = "") => apiClient.get(`/transactions/${studentId}/success?s=${keyword}`);
export const getBills = (studentId: string, keyword: string = "") => apiClient.get(`/transactions/${studentId}/bills?s=${keyword}`);