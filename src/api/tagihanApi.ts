import apiClient from "./apiClient";

export const getTagihanByStudent = (studentId: string) => apiClient.get(`/tagihan/${studentId}`);
